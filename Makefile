deps:
	pip install -r requirements.txt

dump:
	cp notes_app/db_dump/0002_add_default_categories.py notes_app/migrations/

migrations:
	python manage.py makemigrations

migrate:
	python manage.py migrate

admin:
	python manage.py createsuperuser

run: migrations dump migrate
	python manage.py runserver 8001

restart:
	python manage.py runserver 8001
