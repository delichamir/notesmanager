from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.forms import ModelForm

from .models import Notes, Category


class CreateUserForm(UserCreationForm):
    """
        Form using for registration new user
    """
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update({'class': 'textBox'})
        self.fields['email'].widget.attrs.update({'class': 'textBox'})
        self.fields['password1'].widget.attrs.update({'class': 'textBox'})
        self.fields['password2'].widget.attrs.update({'class': 'textBox'})


class NotesForm(ModelForm):
    """
        Form using for adding new note
    """
    heading = forms.CharField(max_length=100, widget=forms.TextInput(attrs={
        "class": "form-control", "placeholder": "Enter Title"
    }))
    content = forms.CharField(max_length=500, widget=forms.Textarea(attrs={
        "class": "form-control", "placeholder": "Enter Notes", "rows": "8"
    }))
    category = forms.ModelChoiceField(queryset=Category.objects.all())
    favorite = forms.BooleanField(required=False)
    uuid = forms.CharField(required=False)

    def validated_data(self, user):
        new_note = Notes(
            id=self.cleaned_data['uuid'],
            heading=self.cleaned_data['heading'],
            content=self.cleaned_data['content'],
            category=self.cleaned_data['category'],
            favorite=self.cleaned_data['favorite'],
            uuid=self.cleaned_data['uuid'],
            user_id=user
        )
        return new_note

    class Meta:
        model = Notes
        fields = ['heading', 'content', 'category', 'favorite', 'uuid']


class DateInput(forms.DateInput):
    """
        Date input for DateForm to view date input in calendar mode
    """
    # changing default text input method to calendar method
    input_type = "date"


class DateFrom(forms.Form):
    """
        Form for filter by date, for user to provide filter date
    """
    date_filed = forms.DateField(widget=DateInput)


class CategoryForm(ModelForm):
    """
        Form for filter by category, for user to provide filter category
    """
    category = forms.ModelChoiceField(queryset=Category.objects.all())

    class Meta:
        model = Category
        fields = ['category']
