from django.urls import path
from . import views


urlpatterns = [
    # account sign up, login, logout
    path('', views.register_page, name='reg'),
    path('login/', views.login_page, name='login'),
    path('logout/', views.logout_user, name='logout'),

    # notes flow
    path('notes/', views.notes, name="notes"),
    path('new_note/', views.new_note, name='new'),
    path('note/<str:pk>', views.note_detail, name='note'),
    path('delete_note/<str:pk>', views.delete_note, name='delete'),

    # search
    path('search_result/', views.search_note, name='search'),

    # filter
    path('filter/date/', views.filter_date, name='date'),
    # path('filter/heading/', views.filter_heading, name='heading'),
    path('filter/category/', views.filter_category, name='category'),
    path('filter/favorites/', views.filter_favorite, name='favorites'),

    # sorting
    path('sort/date/', views.sort_date, name='sort-date'),
    path('sort/category/', views.sort_category, name='sort-category'),
    path('sort/favorites/', views.sort_favorite, name='sort-favorite'),
]
