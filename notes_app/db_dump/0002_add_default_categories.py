# Init default categories

from django.db import migrations


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('notes_app', '0001_initial'),
    ]

    operations = [
        migrations.RunSQL(
            sql=[
                ("INSERT INTO notes_app_category (category) VALUES (%s);", ['Link']),
                ("INSERT INTO notes_app_category (category) VALUES (%s);", ['Note']),
                ("INSERT INTO notes_app_category (category) VALUES (%s);", ['Memo']),
                ("INSERT INTO notes_app_category (category) VALUES (%s);", ['Todo'])
            ],
            reverse_sql=[
                ("DELETE FROM notes_app_category where category=%s;", ['Link']),
                ("DELETE FROM notes_app_category where category=%s;", ['Note']),
                ("DELETE FROM notes_app_category where category=%s;", ['Memo']),
                ("DELETE FROM notes_app_category where category=%s;", ['Todo'])
            ],
        )
    ]
