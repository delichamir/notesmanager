from django.db import models
from uuid import uuid4


class Category(models.Model):
    """
        :param category - категория заметки
    """
    category = models.CharField(max_length=20, default="NEW CATEGORY")

    def __str__(self):
        return self.category


class Notes(models.Model):
    """
    :param heading - заголовок заметки
    :param content - содержимое заметки
    :param time -  дата/время создания заметки
    :param category - категория заметки: [
            :param Link - категория "ссылка"
            :param Note - категория "заметка"
            :param Memo - категория "памятка"
            :param TODO - категория "сделать"
            :param Other - категория "другое"
    ]
    :param favorite - галочка избранное
    :param uuid - uuid доступа к заметке по прямой сcылке
    """
    id = models.CharField(primary_key=True, max_length=50, default=uuid4, serialize=False)
    heading = models.CharField(max_length=200)
    content = models.TextField()
    time = models.DateTimeField(auto_now_add=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    favorite = models.BooleanField(default=False)
    uuid = models.CharField(max_length=50, default="", unique=True)
    user_id = models.CharField(max_length=50, default="admin")

    def __str__(self):
        return self.heading
