import uuid

from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from .forms import NotesForm, CreateUserForm, DateFrom, CategoryForm
from .models import Notes, Category


# account implementation
def register_page(request):
    if request.user.is_authenticated:
        return redirect('notes')
    else:
        form = CreateUserForm
        if request.method == 'POST':
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()

                user = form.cleaned_data.get('username')
                messages.success(request, f'Account was created for user: {user}')
                return redirect('login')

        context = {'form': form}
        return render(request, 'accounts/reg.html', context)


def login_page(request):
    if request.user.is_authenticated:
        return redirect('notes')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('notes')
            else:
                messages.info(request, 'Username or Password is incorrect')

        context = {}
        return render(request, 'accounts/login.html', context)


def logout_user(request):
    logout(request)
    return redirect('login')


# notes flow implementation
@login_required(login_url='login')
def notes(request):
    date_form = DateFrom()
    category_form = CategoryForm()

    notes = Notes.objects.all()
    user_notes = get_user_only_notes(notes, request.user.username)

    # sorting notes by default sort 'date'
    user_notes = user_notes[::-1]
    return render(request, "notes.html",
                  {"notes": user_notes, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def new_note(request):
    date_form = DateFrom()
    category_form = CategoryForm()

    Notes.CATEGORIES = get_last_categories()
    form = NotesForm()

    if request.method == 'POST':
        form = NotesForm(request.POST)

        if form.is_valid():
            provided_uuid = form.cleaned_data['uuid']
            if not provided_uuid:
                form.cleaned_data['uuid'] = str(uuid.uuid4())

            user_id = request.user.username
            form = form.validated_data(user_id)
            form.save()
            return redirect("notes")
    return render(request, "update.html",
                  {"form": form, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def note_detail(request, pk):
    date_form = DateFrom()
    category_form = CategoryForm()
    note = Notes.objects.get(id=pk)
    form = NotesForm(instance=note)

    if request.method == 'POST':
        form = NotesForm(request.POST, instance=note)
        if form.is_valid():
            form.save()
            return redirect("notes")
    return render(request, "update.html",
                  {"note": note, "form": form, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def delete_note(request, pk):
    date_form = DateFrom()
    category_form = CategoryForm()
    note = Notes.objects.get(id=pk)
    form = NotesForm(instance=note)

    if request.method == 'POST':
        note.delete()
        messages.info(request, "The note has been deleted")
    return render(request,
                  "delete.html", {"note": note, "form": form, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def search_note(request):
    date_form = DateFrom()
    category_form = CategoryForm()

    search_text = request.POST['search']
    notes = Notes.objects.filter(heading__icontains=search_text) | Notes.objects.filter(content__icontains=search_text)

    # TODO uncomment to search only by Note heading
    # notes = Notes.objects.filter(heading__icontains=search_text)

    user_notes = get_user_only_notes(notes, request.user.username)
    return render(request, "search.html",
                  {"notes": user_notes, "dateform": date_form, "categoryform": category_form})


# Filter implementation
@login_required(login_url='login')
def filter_date(request):
    date_form = DateFrom()
    category_form = CategoryForm()

    notes = Notes.objects.all()
    user_notes = get_user_only_notes(notes, request.user.username)

    filter_date = request.POST['date_filed']

    filter_by_date_notes = []
    for note in user_notes:
        if note.time.strftime("%Y-%m-%d") == filter_date:
            filter_by_date_notes.append(note)

    return render(request, "notes.html",
                  {"notes": filter_by_date_notes, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def filter_heading(request):
    pass


@login_required(login_url='login')
def filter_category(request):
    provided_category_name = ''
    date_form = DateFrom()
    category_form = CategoryForm()
    notes = Notes.objects.all()
    user_notes = get_user_only_notes(notes, request.user.username)

    if request.method == 'POST':
        for cat in Category.objects.all():
            if cat.id == int(request.POST['category']):
                provided_category_name = cat.category
                break

    notes_filtered_by_category = []
    for note in user_notes:
        if note.category.category == provided_category_name:
            notes_filtered_by_category.append(note)

    return render(request, "notes.html",
                  {"notes": notes_filtered_by_category, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def filter_favorite(request):
    date_form = DateFrom()
    category_form = CategoryForm()

    notes = Notes.objects.filter(favorite__icontains=1)
    user_notes = get_user_only_notes(notes, request.user.username)

    favorite_notes = []
    for note in user_notes:
        if note.favorite:
            favorite_notes.append(note)
    return render(request, "notes.html",
                  {"notes": favorite_notes, "dateform": date_form, "categoryform": category_form})


# Sort implementation
@login_required(login_url='login')
def sort_date(request):
    date_form = DateFrom()
    category_form = CategoryForm()
    notes = Notes.objects.all()
    user_notes = get_user_only_notes(notes, request.user.username)

    # sorting by date of notes, from new to old dates
    user_notes = user_notes[::-1]
    return render(request, "notes.html",
                  {"notes": user_notes, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def sort_category(request):
    date_form = DateFrom()
    category_form = CategoryForm()
    notes = Notes.objects.all()
    user_notes = get_user_only_notes(notes, request.user.username)

    # sorting by category name
    cat = []
    for note in user_notes:
        cat.append(note.category.category)
    cat = sorted(cat)

    sorted_by_category_notes = []
    for cat_name in cat:
        for note in user_notes:
            if note.category.category == cat_name:
                sorted_by_category_notes.append(note)

    return render(request, "notes.html",
                  {"notes": sorted_by_category_notes, "dateform": date_form, "categoryform": category_form})


@login_required(login_url='login')
def sort_favorite(request):
    date_form = DateFrom()
    category_form = CategoryForm()
    notes = Notes.objects.all()
    user_notes = get_user_only_notes(notes, request.user.username)

    favorite_notes = []
    simple_notes = []
    for note in user_notes:
        if note.favorite:
            favorite_notes.append(note)
        else:
            simple_notes.append(note)

    return render(request, "notes.html",
                  {"notes": favorite_notes + simple_notes, "dateform": date_form, "categoryform": category_form})


# addition utils functions
def get_user_only_notes(all_notes, username):
    user_notes = []
    for note in all_notes:
        if note.user_id == username:
            user_notes.append(note)
    return user_notes


def get_last_categories():
    categories = Category.objects.all()

    updated_categories = []
    for cat in categories:
        tmp_cat = (cat.category.lower(), cat.category.upper())
        updated_categories.append(tmp_cat)

    return tuple(updated_categories)
